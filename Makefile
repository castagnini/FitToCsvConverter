
CC=clang
CXX=clang++
CXXFLAGS=-std=c++11 -O3 -I./ -I./FitSDKRelease_20
CFLAGS=-std=c99 -Ofast -I./ -I./FitSDKRelease_20

SDK_OBJ= fit.o fit_accumulated_field.o fit_accumulator.o fit_buffer_encode.o \
fit_buffered_mesg_broadcaster.o fit_buffered_record_mesg_broadcaster.o fit_crc.o \
fit_date_time.o fit_decode.o fit_developer_field.o fit_developer_field_definition.o \
fit_developer_field_description.o fit_encode.o fit_factory.o fit_field.o \
fit_field_base.o fit_field_definition.o fit_mesg.o fit_mesg_broadcaster.o \
fit_mesg_definition.o fit_mesg_with_event_broadcaster.o fit_profile.o \
fit_protocol_validator.o fit_unicode.o

all: ${SDK_OBJ}
	 ${CXX} ${CXXFLAGS} converter.cc *.o -o converter 
	 ${CC} ${CFLAGS} parse_old_csv.c  -o parse_old_csv

clean:
	rm -f *.o converter

%.o: FitSDKRelease_20/%.cpp 
	${CXX} ${CXXFLAGS} -c  $<


