
/*
 *  This is the example provided by the FIT SDK, slightly modified.
 */ 


////////////////////////////////////////////////////////////////////////////////
// The following FIT Protocol software provided may be used with FIT protocol
// devices only and remains the copyrighted property of Dynastream Innovations Inc.
// The software is being provided on an "as-is" basis and as an accommodation,
// and therefore all warranties, representations, or guarantees of any kind
// (whether express, implied or statutory) including, without limitation,
// warranties of merchantability, non-infringement, or fitness for a particular
// purpose, are specifically disclaimed.
//
// Copyright 2008 Dynastream Innovations Inc.
////////////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iomanip>
#include <ctime>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>
#include "fit_decode.hpp"
#include "fit_mesg_broadcaster.hpp"
#include "fit_developer_field_description.hpp"



typedef enum {
 split,swim_stroke,distance,distance_units,total_timer_time,total_timer_time_units,total_cycles,total_calories,field_t_last
} field_t;


class Listener
    : public fit::MonitoringMesgListener
    , public fit::DeveloperFieldDescriptionListener
    , public fit::MesgListener
{
private:
	std::vector<std::string> session_fields ;
	std::vector<std::string> lap_fields ;
	std::vector<std::string> length_fields ;
	std::vector<std::string> stroke_names;
	std::vector<bool> session_has_units;
	std::vector<bool> lap_has_units;
	  std::string intervals; 
	  std::string pool_len; 
      std::string pool_len_unit;
public:
	Listener(){
		session_fields = { "start_time", "total_distance","total_distance_units", 
					"total_timer_time","total_timer_time_units","total_cycles",
					"total_calories", "pool_length" , "pool_length_units"  } ;
		session_has_units = { 0,1,0,1,0,0,0,1,0 };
		lap_fields = { "split","swim_stroke","total_distance","total_distance_units",
					"total_timer_time","total_timer_time_units",
					"total_cycles","total_calories" };
		lap_has_units = { 0,0,1,0,1,0,0,0 };
		length_fields = { "split","swim_stroke","total_distance","total_distance_units",
					"total_timer_time","total_timer_time_units","total_strokes",
					"total_calories" };
		stroke_names = { "Freestyle", "a", "Breaststroke", "c", "Drill", "Mixed"};
	}

	static inline int getPosition(std::string  field_name, std::vector<std::string>& fields_orig ){
	    auto it = std::find(fields_orig.begin(), fields_orig.end(), field_name);
    	if ( it  != fields_orig.end() ){
        	return ( it - fields_orig.begin()) ;
    	}else{
        	return -1 ;
    	} 
	}
	
    static std::string fieldToStr(const fit::FieldBase& field, bool with_u, std::string &unit)
    {
		if (field.GetName() == "start_time" ) {
			const int Dec_31_1989 = 631065600 ;
			std::time_t int_date = Dec_31_1989 + field.GetSINT64Value(0) ;
			char date[255] = {0};
			strftime (date, 255, "%F",localtime(&int_date)); 
			return date;
		}else{	
			std::stringstream ss;
            switch (field.GetType())
            {
            // Get float 64 values for numeric types to receive values that have
            // their scale and offset properly applied.
            case FIT_BASE_TYPE_ENUM:
            case FIT_BASE_TYPE_BYTE:
            case FIT_BASE_TYPE_SINT8:
            case FIT_BASE_TYPE_UINT8:
            case FIT_BASE_TYPE_SINT16:
            case FIT_BASE_TYPE_UINT16:
            case FIT_BASE_TYPE_SINT32:
            case FIT_BASE_TYPE_UINT32:
            case FIT_BASE_TYPE_SINT64:
            case FIT_BASE_TYPE_UINT64:
            case FIT_BASE_TYPE_UINT8Z:
            case FIT_BASE_TYPE_UINT16Z:
            case FIT_BASE_TYPE_UINT32Z:
            case FIT_BASE_TYPE_UINT64Z:
            case FIT_BASE_TYPE_FLOAT32:
            case FIT_BASE_TYPE_FLOAT64:
                ss << field.GetFLOAT64Value(0);
                break;
            case FIT_BASE_TYPE_STRING:
                ss << field.GetSTRINGValue(0).c_str();
                break;
            default:
                break;
            }
            if(with_u) unit =  field.GetUnits().c_str()  ;
			return ss.str(); 
		}
    }

	void msgToSplits(fit::Mesg& mesg,std::vector<std::string> &values, std::vector<std::string>&fields, std::vector<bool>& has_units){
		values.resize(fields.size());
		for (FIT_UINT16 i = 0; i < fields.size(); i++) values[i] = "";	
		  
		for (FIT_UINT16 i = 0; i < (FIT_UINT16)mesg.GetNumFields(); i++){
            int pos = getPosition(mesg.GetFieldByIndex(i)->GetName(), fields);
            if (pos >=0){
                bool with_u = has_units[pos]; 
                std::string unit ;
                fit::Field* field = mesg.GetFieldByIndex(i);
                values[pos] = fieldToStr(*field,  with_u, unit);
                if(with_u) values[pos+1] = unit;
            }
        }
		
	}	
	void printHeader( std::vector<std::string>&fields){
		for (FIT_UINT16 i = 0; i < fields.size(); i++) 
			std::wcout << fields[i].c_str() << L",";	
		 std::wcout << L"\n" ;
	}	

	inline void convertIntToStrokeName(std::string&val){
		int st = stoi(val);
		if(st>=0 && st<=5 )val = stroke_names[st];
	}


    void OnMesg(fit::Mesg& mesg) override
    {
	  static int len_num = 1 ;
	  std::vector<std::string> values;
	  if( mesg.GetName() == "session" ){
			msgToSplits(mesg,values,session_fields,session_has_units);

			pool_len = std::string(values[7].c_str()) ; 
			pool_len_unit = std::string(values[8].c_str());
			std::wcout <<L"Version:1.0\n";
			printHeader(session_fields);
			for (FIT_UINT16 i = 0; i < values.size(); i++){
            	std::wcout << values[i].c_str() <<L",";
        	}
        	 std::wcout << L"\n";
			printHeader(lap_fields);
	  }
	  else if(mesg.GetName() == "lap"){
		msgToSplits(mesg,values,lap_fields,lap_has_units);
		values[split] = "lap";
		len_num = 1;
		
		bool is_rest = (values[swim_stroke] == "" );
		if(is_rest) {
			values[split] = "rest";
			values[swim_stroke] = "Rest";
		}else{
			convertIntToStrokeName(values[swim_stroke]);
		}
		
        for (FIT_UINT16 i = 0; i < field_t_last; i++){
            std::wcout << values[i].c_str() <<L",";
		}
         std::wcout << L"\n";
		
		std::wcout << intervals.c_str();
		intervals = "";
	
      }else if(mesg.GetName() == "length" ){
		msgToSplits(mesg,values,length_fields,lap_has_units);
	  
		values[split] = std::to_string(len_num);
		values[distance] =  pool_len ;
		values[distance+1] = pool_len_unit;
		len_num ++;	
		
		// check if rest
		bool is_rest = (values[swim_stroke] == "" );
		if( is_rest ) return ; // done
			
		convertIntToStrokeName(values[swim_stroke]);
		
        for (FIT_UINT16 i = 0; i < field_t_last; i++){
			intervals += values[i];
			intervals += ","; 
		}
		intervals += "\n"; 


      }//lap, session, len
    }
   void OnDeveloperFieldDescription( const fit::DeveloperFieldDescription& desc ) override
   {
   }

   void OnMesg(fit::MonitoringMesg& mesg) override
   {
   }


};

int main(int argc, char* argv[])
{
   fit::Decode decode;
   fit::MesgBroadcaster mesgBroadcaster;
   Listener listener;
   std::fstream file;

   if (argc != 2)
   {
      printf("Usage: decode.exe <filename>\n");
      return -1;
   }

   file.open(argv[1], std::ios::in | std::ios::binary);

   if (!file.is_open())
   {
      printf("Error opening file %s\n", argv[1]);
      return -1;
   }

   if (!decode.CheckIntegrity(file))
   {
      printf("FIT file integrity failed.\nAttempting to decode...\n");
   }

   mesgBroadcaster.AddListener((fit::MonitoringMesgListener &)listener);
   mesgBroadcaster.AddListener((fit::MesgListener &)listener);

   try
   {
      decode.Read(&file, &mesgBroadcaster, &mesgBroadcaster, &listener);
   }
   catch (const fit::RuntimeException& e)
   {
      printf("Exception decoding file: %s\n", e.what());
      return -1;
   }


   return 0;
}

