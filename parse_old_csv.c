

#include<stdio.h>
#include<stdlib.h>

#include "parse_old_csv.h"

char* tokenize_line(char*line, char*tokens, int tok_size, int maxlen, int *found){

	*found = 0 ;
	if(*line == '\0') return NULL;

	char*tok_ptr = tokens ; // + *found * maxlen; // found is 0 here
	char*ptr = line;
	int esc =0;

	memset(tokens,0,tok_size);

	while(*ptr == ' '|| *ptr == '\t') ptr++; //remove trailing spaces
    
	while(1){
        if(*ptr == '"'){
            esc = !esc ;
			ptr++;
			continue;
        }
        else if(*ptr == '\n' || *ptr == '\0'){
			// end of line = end of while.
			// token found if line was not empty.
			*tok_ptr = '\0';
			(*found)++;
			ptr++;
			break;
        }else if(esc && *ptr == ','){
			ptr++; // removes commas in "1,000"
            continue;
        }else if(!esc && *ptr == ','){
			// token found if line was not empty.
			*tok_ptr = '\0';
			(*found)++;
			tok_ptr = tokens + (*found) * maxlen;
			ptr++;
			while(*ptr == ' '|| *ptr == '\t') ptr++; //remove trailing spaces
			continue;
		}else{
        	*tok_ptr = *ptr ;
			tok_ptr++;
       		ptr++;
		}
    }
	
	return ptr;

}

char*read_file(char*filename, file_info_p info){
        FILE * fp;
        if( (fp=fopen(filename,"r") ) == 0){
                fprintf(stderr,"Could not open file: %s\n",filename);
                exit(1);
        }
		
		off_t filesize = get_filesize(fp);
		// put a check on filesize < 1Mb;
		char*buf = malloc((size_t)filesize+1);	
		fread(buf, 1, filesize, fp);
		fclose(fp);
		buf[filesize] = '\0';

        unsigned long n_lines = 0, c=0, s=0, line_size_max=0,tokens_max=0;
        int i,esc=0;
        char*ptr;
        // count lines
        ptr = buf;
		i = (size_t)filesize;
        while(i--){
			c++;
			if(*ptr == '"'){
				esc = !esc ;
			}
        	else if(*ptr == '\n'){
				if(esc){
					 fprintf (stderr,"Error: escaped sequence not closed within a line. I stop here\n");
					exit(1);
				}
           		n_lines++;
				if(c>line_size_max) line_size_max = c;
				if(s>tokens_max) tokens_max = s;
				c=0;
				s=0;
			}else if(!esc && *ptr == ','){
				s++;	
			}
        	++ptr;
			
        }
		if(c>0 && c>line_size_max) line_size_max = c;
		if(s>0 && s>tokens_max) tokens_max = s;
	//	if(esc) { fprintf (stderr,"Error: escaped sequence not closed.\n");}
	//	printf("n_lines:%lu\nmax charactersper line:%lu\nmax tokens per line:%lu\n",n_lines,line_size_max,tokens_max);

		info->filesize = (unsigned long)filesize;
		info->n_lines = n_lines;
		info->line_size = line_size_max+1;
		info->tokens = tokens_max+1;
        return buf;
}

#define DISTANCE_IDX 3
#define TIME_IDX 4
#define STYLE_IDX 1

void append_summary(str_t * outbuf,char*tokens,int stroke_idx,int kcal_idx,int line_size){
	char buf[32];
	append_string(outbuf,&tokens[DISTANCE_IDX*line_size]);
	append_string(outbuf,",m,");
	double time = str_to_seconds(&tokens[TIME_IDX*line_size]); 
	snprintf(buf,32,"%.3lf",time);
	append_string(outbuf,buf);
	append_string(outbuf,",s,");
	if(tokens[stroke_idx*line_size] != '-')
	append_string(outbuf,&tokens[stroke_idx*line_size]);
	append_string(outbuf,",");
	if(kcal_idx>=0  && tokens[kcal_idx*line_size] != '-' ){
		append_string(outbuf,&tokens[kcal_idx*line_size]);
	}
	append_string(outbuf,",");

}
void append_lap(str_t * outbuf,char*tokens,int len_counter,char*course,int stroke_idx,int kcal_idx,int line_size){
	char buf[32];
	if(len_counter == 0){
		append_string(outbuf,"Lap,");
	}else{
		snprintf(buf,32,"%d",len_counter);
		append_string(outbuf,buf);
		append_string(outbuf,",");
	}
	append_string(outbuf,&tokens[STYLE_IDX*line_size]);
	append_string(outbuf,",");
	append_string(outbuf,&tokens[DISTANCE_IDX*line_size]);
	append_string(outbuf,",m,");
	double time = str_to_seconds(&tokens[TIME_IDX*line_size]); 
	snprintf(buf,32,"%.3lf",time);
	append_string(outbuf,buf);
	append_string(outbuf,",s,");
	if(tokens[stroke_idx*line_size] != '-')
		append_string(outbuf,&tokens[stroke_idx*line_size]);
	append_string(outbuf,",");
	if(kcal_idx>=0 && tokens[kcal_idx*line_size] != '-')
		append_string(outbuf,&tokens[kcal_idx*line_size]);
	append_string(outbuf,"\n");
}

void append_rest(str_t * outbuf,char*tokens,int line_size){
	char buf[32];
	append_string(outbuf,"Rest,Rest,,,");
	double time = str_to_seconds(&tokens[TIME_IDX*line_size]); 
	snprintf(buf,32,"%.3lf",time);
	append_string(outbuf,buf);
	append_string(outbuf,",s,,\n");
}

void parse_csv(char*filename ){
	file_info_t info;
	char*buf = read_file(filename, &info);
	long token_buf_size = info.line_size*info.tokens;
	char*tokens = malloc(token_buf_size);
	int tok_found=0;
	str_t outbuf ;
	str_t temp_str ;
	alloc_string(&outbuf);
	alloc_string(&temp_str);

	char* courses[] = {"50,m", "25,m", "20,m"};
	char* pcourse ;
	
	append_string(&outbuf,"Version:1.0\n");
	append_string(&outbuf,"start_time,total_distance,total_distance_units,"
           "total_timer_time,total_timer_time_units,total_cycles,"
           "total_calories,pool_length,pool_length_units\n");

	append_string(&outbuf,strtok(filename,"_"));
	append_string(&outbuf,",");
	strtok(NULL, "_");
	char*ptr = strtok(NULL, "_");		
	if(ptr[0] == '5') 
		pcourse = courses[0]; 
	else if(ptr[1] == '5')
		pcourse = courses[1]; 
	else
		pcourse = courses[2]; 


	char* nextline = tokenize_line(buf,tokens,token_buf_size,info.line_size,&tok_found);	

	int stroke_idx = get_column_for_header_key(tokens, info.line_size, info.tokens, "Total Strokes")	;
	if(stroke_idx == -1) stroke_idx = get_column_for_header_key(tokens, info.line_size, info.tokens, "sumStrokes")	;
	
	int kcal_idx = get_column_for_header_key(tokens, info.line_size, info.tokens, "Calories") ; 
	int len_counter = 0 ;	
	while((nextline = tokenize_line(nextline,tokens,token_buf_size,info.line_size,&tok_found)) != NULL)	
	{
		if(strncmp(tokens,"Summary",5)==0) {
			append_summary(&outbuf,tokens,stroke_idx,kcal_idx,info.line_size);	
			append_string(&outbuf,pcourse);
			append_string(&outbuf,"\nsplit,swim_stroke,total_distance,total_distance_units"
                    "total_timer_time,total_timer_time_units,"
                    "total_cycles,total_calories");
			continue;
		}
		if(strncmp(tokens,"Rest",4)==0) {
			len_counter = 0;
			append_rest(&temp_str,tokens,info.line_size);				
			continue;
		}
		linetype_t lt = get_line_type(tokens);	
		if(lt == is_lap){
			len_counter = 0;
			append_lap(&temp_str,tokens,len_counter++,pcourse,stroke_idx,kcal_idx,info.line_size);	
			continue;
		}else if (lt == is_len) {
			append_lap(&temp_str,tokens,len_counter++,pcourse,stroke_idx,kcal_idx,info.line_size);	
		
			continue;	
		}
		
	}
	puts(outbuf.base);
	fputs(temp_str.base,stdout);
	destroy_string(&temp_str);	
	destroy_string(&outbuf);	
	free(tokens);	
	free(buf);	

}


int main (int argc, char*argv[]){

	parse_csv(argv[1]);
	return 0 ; 
}







