#ifndef _PARSE_CSV_H_
#define _PARSE_CSV_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

typedef struct {
	unsigned long filesize;
	unsigned long n_lines;
	unsigned long line_size;
	unsigned long tokens;
	// error info
} file_info_t, *file_info_p;

static inline off_t get_filesize(FILE*fp){
	fseeko(fp, 0, SEEK_END);
	off_t sz = ftello(fp);
	// rewind;
	fseeko(fp, 0, SEEK_SET);
	return sz;
}


// tokens is maxlen*total_tokens
char*read_file(char*filename, file_info_p info);

char* tokenize_line(char*line, char*tokens, int tok_size, int maxlen, int *found);


static inline int get_column_for_header_key (char * tokens,int line_size, int num_toks, char* key)
{
    int res = -1;
    for(int i =0 ; i < num_toks; i ++ ){
        if ( strcmp(key, &tokens[i*line_size])==0 ) {
            res = i;
            break;
        }
    }
    return res;
}

typedef struct {
	char* base;
	char* cur;
	long size;
} str_t ;

#define STR_INC 4096
static inline char * alloc_string (str_t * s){
	s->base = malloc(STR_INC);
	s->cur = s->base;
	*(s->cur) = 0;
	s->size = STR_INC;
	return s->base;
}

static inline void destroy_string (str_t * s){
	free(s->base);
}

static inline void append_string (str_t * dest, char*src ){
	int len = strlen(src);  
	int inc = STR_INC > len? STR_INC:len;
	long pos = dest->cur - dest->base;
	if( pos > dest->size - len -1 ){
		dest->base = realloc(dest->base, dest->size+inc); 
		dest->cur  = dest->base+pos;
		dest->size = dest->size+inc;
    }
	strcat(dest->cur,src);
	dest->cur  += len ;
}

static inline double  str_to_seconds(char *s){
    double min = 0.;
    double hours = 0. ;
    double sec = 0. ;
    double parts[3] = {0.,0.,0.};
    int found =0;

    for (char *p = strtok(s,":"); p != NULL && found < 3 ; p = strtok(NULL, ":"))
    {
        parts[found] = atof(p);
        found ++ ;
    }

    if ( found == 1 ){
        sec = parts[0];
    }
    else if ( found == 2 ){
        min = parts[0];
        sec = parts[1];
    }
    else{
        hours = parts[0];
        min = parts[1];
        sec = parts[2];
    }
    return ( sec + min*60. + hours*3600.);
}

typedef enum{
	is_lap,
	is_len,
	is_unknown,
} linetype_t;
 
static inline linetype_t get_line_type(char*tok){
	bool pnt_fnd =false;
	bool num_fnd = false;
	while( *tok != 0 ){
		if(!num_fnd && *tok <= '9' && *tok >= '0'){
			num_fnd = true;
			continue;
		}
		if(*tok++ == '.'){
			pnt_fnd = true;
			break;
		}
	}
	if(!num_fnd) return is_unknown;
	if(!pnt_fnd) return is_lap;
	return is_len;
}

void parse_csv(char*filename);

#endif


